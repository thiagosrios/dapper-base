# Dapper Base .Net Core

Este repositório contém uma estrutura inicial para projetos [.NET Core](https://docs.microsoft.com/pt-br/aspnet/core/?view=aspnetcore-2.2), usando o *microframework* [Dapper](https://github.com/StackExchange/Dapper) na camada de dados (ORM).

A escolha do **Dapper** é pautada em questões de performance, pois a utilização de ORM's como o *EntityFramework* reduz a perfomance de aplicações que necessitam manipular grandes quantidades de dados em pequenos intervalos de tempo. Alguns links contendo testes de performances demonstram isso:

- [Utilizando o Micro ORM Dapper em uma aplicação ASP.NET Core](https://www.treinaweb.com.br/blog/utilizando-o-micro-orm-dapper-em-uma-aplicacao-asp-net-core/)

- [Using Dapper Asynchronously in ASP.NET Core 2.1](https://exceptionnotfound.net/using-dapper-asynchronously-in-asp-net-core-2-1/)

O *Dapper* deve ser utilizado em bancos que já possuam estrutura criada. Para mecanismos de migrations ou criação de tabelas via código, é necessário implementar classes específicas para esta tarefa. Este projeto irá criar, por padrão, a tabela *'values'* para inicialização da aplicação no banco de dados definido.


## Pré-requisitos

- [.Net Core](https://dotnet.microsoft.com/download)
- Visual Studio ou [Visual Studio Code](https://code.visualstudio.com/Download?wt.mc_id=DotNet_Home)


## Configuração do ambiente

Após clonar o repositório, entre na pasta do projeto e delete a pasta *.git*, para iniciar um novo projeto a partir dessa estrutura inicial.

### Passo 1: Configuração do banco de dados

Configure o banco de dados que será usado através dos arquivos *appsettings.json*. Copie esse arquivo e renomeie para *appsettings.Development.json* para utilização no ambiente local de desenvolvimento. 

Defina o *driver* padrão que deverá ser usado (SQL Server, PostgreSQL, MySQL, etc), de acordo com os *drivers* presentes no arquivo *Enums/DatabaseTypes*: 

```json
    "Database": {
        "Driver": "driver"
    }
```

Em seguida, defina a *string* de conexão, de acordo com o *driver* estabelecido: 


```json
    "ConnectionStrings": {
        "DefaultConnection": "MyConnectionString"
    }
```

### Passo 2: Executar a aplicação

Execute o comando: 

`$ dotnet run`

Esse comando irá instalar as dependências (dentro do arquivo *DapperBase.csproj*), gerar o *build* do projeto e iniciar o servidor embutido do IIS. 


### Passo 3: Configurando arquivo de acesso aos dados

Nesta etapa, a aplicação deverá estar em funcionamento de acordo com os passos anteriores. Para estruturar uma aplicação a partir desse repositório base, é necessário criar algumas arquivos nas camadas do projeto. Como este projeto utiliza o Dapper, é necessário que exista uma base criada e com tabelas configuradas. A partir disso crie os arquivos: 

- Model: mapeamento da tabela do banco através da classe, incluindo validações, etc;
- Repository: implementação das funcionalidades de CRUD a partir das queries;
- Controler: classe para acionar as operações no banco, por meio de métodos que expõem o padrão REST

Para configuração de uma nova camada para um entidade específica, execute os seguintes passos: 

1. Crie o arquivo model, com o nome da tabela que deverá ser mapeada, dentro da pasta 'Models'; 
2. Cria o arquivo repository, dentro da pasta 'Repositories', extendendo BaseRepository<T>, onde T é o tipo de classe usado na instância do BaseRepository; 
3. Crie o arquivo de controller, dentro da pasta 'Controllers', de acordo com o padrão 'ValuesController';
4. Adicione a importação do repository récem-criado para injeção de dependência, no arquivo 'Startup.cs', na raiz do projeto, da seguinte forma:


```csharp
public void ConfigureServices(IServiceCollection services)
{
    services.AddTransient<NovoRepository>();
    ... 
}
```

## Adicionando novo *driver* para acesso à dados

Caso seja necessário adicionar uma conexão diferente da estabelecida, cria uma nova entrada no arquivo 'DataBaseType', adicione uma nova entrada no método *GetConnection()* dentro de *Database/ConnectionFactory*, de acordo com o exemplo abaixo: 

- 'Enums/DataBaseType': 

```csharp
        ..., 
        MongoDB
    }
```

- 'Database/ConnectionFactory':

```csharp
    case DataBaseType.MongoDB: 
        return new MongoClient(_configuration.GetConnectionString("MongoDbConnection")); 
```

Em seguida, adicione a *ConnectionString* apropriada para o driver, da seguinte forma:

```json
    "ConnectionStrings": {
        "MongoDbConnection": "mongodb://localhost:27017"
    },
```