using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DapperBase.Models 
{
    [Table("values")]
    public class Value 
    {
        public int ID { get; set; } 

        [Required(ErrorMessage="O nome é obrigatório")]
        public string name { get; set; }

        [Required(ErrorMessage="O valor é obrigatório")]
        public string value { get; set; }

    }
}