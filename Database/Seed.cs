using System.Data;
using System.Data.SqlClient;
using Dapper; 
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace DapperBase.Database 
{
    public class Seed : ConnectionFactory
    {   

        public Seed(IConfiguration config) : base(config) {} 

        public void CreateDB()
        {
            using (IDbConnection conn = GetConnection())
            {
                string sql = @"CREATE TABLE IF NOT EXISTS values (
                                id serial NOT NULL, 
                                name character varying(100) NOT NULL,
                                value character varying(100) NOT NULL, 
                                CONSTRAINT pk_values PRIMARY KEY (id) 
                            )";
                conn.Open();
                var result = conn.Execute(sql);
                conn.Close(); 
            }
        }
    }
}