using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using Npgsql;
using DapperBase.Enums;

namespace DapperBase.Database 
{
    public class ConnectionFactory
    {
        // Propriedade para obter informações da configuração
        private readonly IConfiguration _configuration;
        private string _type;

        /*
         * Método construtor para injeção de dependência de configurações
         */
        public ConnectionFactory(IConfiguration config) 
        {
            _configuration = config; 
            _type = _configuration.GetSection("Database").GetSection("Driver").Value; 
        }

        /*
         * Retorna instância de conexão de acordo com as configurações de banco
         */
        public IDbConnection GetConnection()
        {
            Enum.TryParse(_type, out DataBaseType database);

            switch(database) {
                case DataBaseType.SQLServer: 
                    return new SqlConnection(_configuration.GetConnectionString("SQLServerConnection"));         
                case DataBaseType.PostgreSQL: 
                    return new NpgsqlConnection(_configuration.GetConnectionString("PgConnection"));    
                case DataBaseType.MySQL: 
                    return new MySqlConnection(_configuration.GetConnectionString("MySqlConnection"));
                default: 
                    return new SqlConnection(_configuration.GetConnectionString("DefaultConnection"));
            }
        }

    }
}