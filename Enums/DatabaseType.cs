namespace DapperBase.Enums 
{
        public enum DataBaseType
        {
            MySQL,
            PostgreSQL,
            SQLite,
            SQLServer
        }
}