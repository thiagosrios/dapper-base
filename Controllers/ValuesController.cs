﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DapperBase.Repositories; 
using DapperBase.Models; 

namespace DapperBase.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private ValuesRepository _valuesRepository;

        /*
         * Método construtor: injeção de dependência do repository    
         */
        public ValuesController(ValuesRepository valuesRepository)
        {
            _valuesRepository = valuesRepository;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<Value> Index()
        {
            return _valuesRepository.FindAll(); 
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<Value> Get(int id)
        {
            var value = _valuesRepository.FindByID(id); 

            if(value == null) {
                return NotFound(); 
            }

            return value; 
        }

        // POST api/values
        [HttpPost]
        public ActionResult<Value> Post(Value value)
        {
            if (ModelState.IsValid)
            {
                var newValue = _valuesRepository.Add(value);
                return CreatedAtAction("Get", new { id = newValue.ID }, value);
            }

            return value;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public ActionResult<Value> Put(int id, Value value)
        {
            if (ModelState.IsValid)
            {
                _valuesRepository.Update(id, value);
                return CreatedAtAction("Get", new { id = id }, value);
            }

            return value;
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public ActionResult<Value> Delete(int id)
        {
            var value = _valuesRepository.FindByID(id);
            if(value == null) {
                return NotFound();
            }

            _valuesRepository.Delete(id);
            return value;
        }
    }
}
