using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Collections.Generic;
using Dapper; 
using Microsoft.Extensions.Configuration;
using Npgsql;
using DapperBase.Database;

/*
 * Classe base com funcionalidades padronizadas para acesso aos dados
 */
namespace DapperBase.Repositories 
{
    public class BaseRepository<T> : AbstractRepository<T> 
    {
        /// <summary>
        /// Método construtor para injeção de dependência de configurações
        /// </summary>
        public BaseRepository(IConfiguration config) : base(config){}

        /// <summary>
        /// Retorna registro por id
        /// </summary>
        public override T FindByID(int id, string query) {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                var result = conn.Query<T>(query, new { Id = id }); 
                conn.Close(); 
                return result.FirstOrDefault(); 
            } 
        }

        /// <summary>
        /// Retorna registros de uma entidade / tabela
        /// </summary>
        public override IEnumerable<T> FindAll(string query) {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                var result = conn.Query<T>(query).AsList();
                conn.Close(); 
                return result; 
            }
        }

        /// <summary>
        /// Adicionar registro em uma entidade / tabela, através do bind do item com a query
        /// </summary>
        public override T Add(string query, T item) {
            return this.ExecuteQuery(query, item);
        }

        /// <summary>
        /// Atualiza registro de uma entidade / tabela, através do bind do item com a query
        /// </summary>
        public override void Update(string query, T item) {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                var result = conn.Query(query, item);
                conn.Close(); 
            }
        }

        /// <summary>
        /// Exclui registro da tabela alvo
        /// </summary>
        public override void Remove(string query, int id) {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                var result = conn.Execute(query, new { Id = id });
                conn.Close(); 
            }
        }
        
        /// <summary>
        /// Função genérica para execução de queries, sem retorno de resultados
        /// </summary>
        private T ExecuteQuery(string query, T item) {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                var result = conn.Execute(query, item);
                conn.Close(); 
                return item;
            }
        }

    }
}