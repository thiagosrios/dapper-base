using Microsoft.Extensions.Configuration;
using DapperBase.Models;
using DapperBase.Database;
using System.Collections.Generic;

namespace DapperBase.Repositories {

    public class ValuesRepository : BaseRepository<Value> 
    {
        public ValuesRepository(IConfiguration config) : base(config)
        {
            Seed seed = new Seed(config); 
            seed.CreateDB(); 
        }

        public Value FindByID(int id) 
        {
            string sql = "SELECT * FROM values WHERE id = @ID"; 
            return FindByID(id, sql); 
        }

        public IEnumerable<Value> FindAll() 
        {
            string sql = "SELECT * FROM values"; 
            return FindAll(sql); 
        }

        public Value Add(Value value)
        {
            string sql = "INSERT INTO values (name, value) VALUES(@name, @value) ";
            return base.Add(sql, value); 
        }

        public void Update(int id, Value value)
        {
            string sql = "UPDATE values SET name = @name, value = @value " + 
                         "WHERE Id = @Id ";
            base.Update(sql, value); 
        }

        public void Delete(int id)
        {
            string sql = "DELETE FROM values WHERE id = @id";
            base.Remove(sql, id); 
        }
    }

}