using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Collections.Generic;
using Dapper; 
using Microsoft.Extensions.Configuration;
using Npgsql;
using DapperBase.Database;

/*
 * Classe abstrata para criação de repositories de um model usado na aplicação
 */
namespace DapperBase.Repositories 
{
    public abstract class AbstractRepository<T> : ConnectionFactory
    {
        /// <summary>
        /// Método construtor para injeção de dependência de configurações
        /// </summary>
        public AbstractRepository(IConfiguration config) : base(config){}

        /// <summary>
        /// Retorna registro por id
        /// </summary>
        public abstract T FindByID(int id, string query);

        /// <summary>
        /// Retorna registros de uma entidade / tabela
        /// </summary>
        public abstract IEnumerable<T> FindAll(string query);

        /// <summary>
        /// Adicionar registro em uma entidade / tabela, através do bind do item com a query
        /// </summary>
        public abstract T Add(string query, T item);

        /// <summary>
        /// Atualiza registro de uma entidade / tabela, através do bind do item com a query
        /// </summary>
        public abstract void Update(string query, T item);

        /// <summary>
        /// Exclui registro da tabela alvo
        /// </summary>
        public abstract void Remove(string query, int id);
        
    }
}